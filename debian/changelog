quiterss (0.19.4+dfsg-1) unstable; urgency=medium

  * New upstream release.

 -- Dmitry Smirnov <onlyjob@debian.org>  Sun, 03 May 2020 11:40:09 +1000

quiterss (0.19.3+dfsg-1) unstable; urgency=medium

  * New upstream release.
    + fixed application crash (Closes: #949686, #949894).
  * Standards-Version: 4.5.0

 -- Dmitry Smirnov <onlyjob@debian.org>  Tue, 04 Feb 2020 15:17:05 +1100

quiterss (0.19.2+dfsg-1) unstable; urgency=medium

  * New upstream release.
  * source/options: removed compression override.

 -- Dmitry Smirnov <onlyjob@debian.org>  Sat, 30 Nov 2019 20:26:34 +1100

quiterss (0.19.1+dfsg-1) unstable; urgency=medium

  * New upstream release.
  * Standards-Version: 4.4.1.
  * DH & compat to version 12.

 -- Dmitry Smirnov <onlyjob@debian.org>  Mon, 18 Nov 2019 11:02:43 +1100

quiterss (0.18.12+dfsg-1) unstable; urgency=medium

  * New upstream release (Closes: #889845, #873564).
  * Standards-Version: 4.1.4.
  * Vcs URLs to Salsa.

 -- Dmitry Smirnov <onlyjob@debian.org>  Thu, 05 Jul 2018 16:35:21 +1000

quiterss (0.18.8+dfsg-1) unstable; urgency=medium

  * New upstream release [August 2017].
  * Debhelper/compat to version 10.
  * Standards-Version: 4.1.1.

 -- Dmitry Smirnov <onlyjob@debian.org>  Mon, 09 Oct 2017 00:50:23 +1100

quiterss (0.18.6+dfsg-1) unstable; urgency=medium

  * New upstream release [June 2017].

 -- Dmitry Smirnov <onlyjob@debian.org>  Sat, 24 Jun 2017 22:14:53 +1000

quiterss (0.18.4+dfsg-2) unstable; urgency=medium

  * Standards-Version: 3.9.8
  * Vcs-Git URL to HTTPS
  * Copyright format URL to HTTPS
  * Added "debian/clean" file to remove generated .qm files.
  * dbgsym-migration: dropped -dbg package
  * rules: use "lrelease" instead of "lupdate" (Closes: #835500)
    Thanks, Lisandro Damián Nicanor Pérez Meyer.

 -- Dmitry Smirnov <onlyjob@debian.org>  Sun, 13 Nov 2016 06:56:28 +1100

quiterss (0.18.4+dfsg-1) unstable; urgency=medium

  * New upstream release [March 2016].
  * Standards-Version: 3.9.7.
  * Updated Vcs-Browser URL.

 -- Dmitry Smirnov <onlyjob@debian.org>  Sun, 20 Mar 2016 18:24:48 +1100

quiterss (0.18.3+dfsg-1) unstable; urgency=medium

  * New upstream release [January 2016].
  * Dropped obsolete "fix-FTBFS-qt5.5.patch".

 -- Dmitry Smirnov <onlyjob@debian.org>  Mon, 01 Feb 2016 20:26:42 +1100

quiterss (0.18.2+dfsg-2) unstable; urgency=medium

  * New patch to fix FTBFS (Closes: #805240).

 -- Dmitry Smirnov <onlyjob@debian.org>  Sun, 20 Dec 2015 20:07:21 +1100

quiterss (0.18.2+dfsg-1) unstable; urgency=low

  * New upstream release [July 2015].
  * Switch to QT5 (Closes: #784520).
  * Dropped obsolete "qrc.patch".
  * Build-Depends:
    - libphonon-dev
    - libqt4-dev
    - libqtwebkit-dev
    - qt4-linguist-tools
    + qtmultimedia5-dev
    + libqt5webkit5-dev
    + qttools5-dev-tools
  * Depends:
    - libqt4-sql-sqlite
    + libqt5sql5-sqlite

 -- Dmitry Smirnov <onlyjob@debian.org>  Tue, 14 Jul 2015 17:56:23 +1000

quiterss (0.17.7+dfsg-1) unstable; urgency=low

  * New upstream release [April 2015].
  * Build-Depends:
    - qt4-qmake
  * watch:
    + switch to GitHub monitoring.
    + added repacksuffix.
  * rules: removed get-orig-source in favour of new `uscan`
    "Files-Excluded" filtering.
  * copyright/Files-Excluded: do not drop *.ico files.
  * New "qrc.patch" to replace .ico with .png file.
  * Upload to unstable.

 -- Dmitry Smirnov <onlyjob@debian.org>  Wed, 20 May 2015 03:17:25 +1000

quiterss (0.17.6+dfsg-1) experimental; urgency=low

  * New upstream release [February 2015].
  * copyright: added Files-Excluded section.

 -- Dmitry Smirnov <onlyjob@debian.org>  Sat, 14 Mar 2015 13:27:15 +1100

quiterss (0.17.3+dfsg-1) experimental; urgency=low

  * New upstream release [January 2015].

 -- Dmitry Smirnov <onlyjob@debian.org>  Tue, 06 Jan 2015 13:38:20 +1100

quiterss (0.17.2+dfsg-1) experimental; urgency=medium

  * New upstream release [December 2014].
  * Standards-Version: 3.9.6.

 -- Dmitry Smirnov <onlyjob@debian.org>  Thu, 01 Jan 2015 15:58:27 +1100

quiterss (0.17.0+dfsg-1) unstable; urgency=low

  * New upstream release [September 2014].

 -- Dmitry Smirnov <onlyjob@debian.org>  Tue, 09 Sep 2014 17:50:33 +1000

quiterss (0.16.2+dfsg-1) unstable; urgency=medium

  * New upstream release [August 2014].

 -- Dmitry Smirnov <onlyjob@debian.org>  Tue, 19 Aug 2014 11:10:21 +1000

quiterss (0.16.1+dfsg-1) unstable; urgency=low

  * New upstream release [July 2014].
  * Added "debian/gbp.conf".

 -- Dmitry Smirnov <onlyjob@debian.org>  Sun, 20 Jul 2014 14:05:10 +1000

quiterss (0.16.0+dfsg-1) unstable; urgency=low

  * New upstream release [May 2014].
  * "debian/rules": updated get-orig-source to keep "3rdparty/sqlitex".

 -- Dmitry Smirnov <onlyjob@debian.org>  Sun, 25 May 2014 15:27:49 +1000

quiterss (0.15.4+dfsg-1) unstable; urgency=low

  * New upstream release [April 2014].

 -- Dmitry Smirnov <onlyjob@debian.org>  Sun, 27 Apr 2014 20:47:23 +1000

quiterss (0.15.3+dfsg-1) unstable; urgency=low

  * New upstream release [April 2014].

 -- Dmitry Smirnov <onlyjob@debian.org>  Tue, 08 Apr 2014 21:31:42 +1000

quiterss (0.15.2+dfsg-1) unstable; urgency=low

  * New upstream release [March 2014].
    - dropped "next-unread.patch".
  * "debian/watch" to check new downloads location at quiterss.org.
  * Standards to 3.9.5.
  * Homepage to "http://quiterss.org".
  * Build-Depends:
    + libphonon-dev

 -- Dmitry Smirnov <onlyjob@debian.org>  Sat, 29 Mar 2014 17:15:24 +1100

quiterss (0.13.3+dfsg-2) unstable; urgency=low

  * Added -dbg package.
  * New backported patch to fix segfault on 'next unread' (Closes: #720318).

 -- Dmitry Smirnov <onlyjob@debian.org>  Mon, 14 Oct 2013 23:44:24 +1100

quiterss (0.13.3+dfsg-1) unstable; urgency=low

  * New upstream release [August 2013].

 -- Dmitry Smirnov <onlyjob@debian.org>  Sat, 07 Sep 2013 13:39:16 +1000

quiterss (0.13.2+dfsg-1) unstable; urgency=low

  * New upstream release [July 2013].
  * Dropped "mime.patch" (applied-upstream).
  * Updated information about QuiteRSS/bug-132 in "README.Debian".

 -- Dmitry Smirnov <onlyjob@debian.org>  Sat, 10 Aug 2013 11:01:56 +1000

quiterss (0.13.1+dfsg-1) unstable; urgency=low

  * Initial release (Closes: #714818).

 -- Dmitry Smirnov <onlyjob@debian.org>  Wed, 03 Jul 2013 16:31:03 +1000
